﻿using Constellation;
using Constellation.Control;
using Constellation.Host;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBrain
{
    public class Program : PackageBase
    {
        [StateObjectLink("SFridge", "Content")]
        public StateObjectNotifier fridgecontent { get; set; }
        
        [StateObjectLink("SFridge", "Tobuy")]
        public StateObjectNotifier shoplist { get; set; }

        [StateObjectLink("SFridge", "Count")]
        public StateObjectNotifier count { get; set; }

        [StateObjectLink("MatchOfTheDay", "MOTD")]
        public StateObjectNotifier motd { get; set; }

        [StateObjectLink("MatchOfTheDay", "MOTD_favteam")]
        public StateObjectNotifier favteam { get; set; }

        public bool isMatchToday { get; set; }
        public string match { get; set; }

        static void Main(string[] args)
        {
            PackageHost.Start<Program>(args);
        }

        public override void OnStart()
        {
            PackageHost.WriteInfo("Package starting - IsRunning: {0} - IsConnected: {1}", PackageHost.IsRunning, PackageHost.IsConnected);

            if (!PackageHost.HasControlManager)
            {
                PackageHost.WriteError("Unable to connect");
                return;
            }

            PackageHost.ControlManager.RegisterStateObjectLinks(this);
            this.isMatchToday = false;
            this.shoplist.ValueChanged += shoplist_ValueChanged;
            this.count.ValueChanged += count_ValueChanged;
            this.motd.ValueChanged += motd_ValueChanged;
        }
        void motd_ValueChanged(object sender, StateObjectChangedEventArgs e)
        {
            // si l'équipe favorite est dans le programme => notif
            if(!e.IsNew)
            {
                if (!JToken.DeepEquals((JArray)e.OldState.Value, (JArray)e.NewState.Value))
                //if((JArray)e.NewState.Value != (JArray)e.OldState.Value)
                {
                    this.isMatchToday = false;
                    this.match = null;
                    JArray value = (JArray)e.NewState.Value;
                    foreach(var item in value){
                        if (((string)item).Contains((string)this.favteam.DynamicValue))
                        {
                            this.isMatchToday = true;
                            this.match = (string)item;
                        }
                    }
                    if(this.isMatchToday && ((long)this.count.DynamicValue < PackageHost.GetSettingValue<long>("minBeerFootballGame")))
                    {
                        //* Send push bullet alert
                        PackageHost.CreateScope("PushBullet").Proxy.SendPush(
                           new
                           {
                               Title = "Breaking beer !",
                               Message = string.Format("{0} ! Don't forget the beer ({1} in stock)", this.match, this.count.DynamicValue)
                           });
                    }
                }
            }
        }

        void shoplist_ValueChanged(object sender, StateObjectChangedEventArgs e)
        {
            if (!e.IsNew)
            {
                dynamic oldState = JObject.Parse((string)e.OldState.Value);
                dynamic newState = JObject.Parse((string)e.NewState.Value);
                if(newState.count > oldState.count){
                    //* Send push bullet alert
                    PackageHost.CreateScope("PushBullet").Proxy.SendPush(
                       new
                       {
                           Title = "Shopping list",
                           Message = "New product in the shopping list"
                       });
                    //*/
                }
            }
        }

        void count_ValueChanged(object sender, StateObjectChangedEventArgs e)
        {
            if (!e.IsNew)
            {
                if ((long)e.NewState.Value != (long)e.OldState.Value)
                {
                    long reference;
                    string message;
                    if (this.isMatchToday)
                    {
                        reference = PackageHost.GetSettingValue<long>("minBeerFootballGame");
                        message = string.Format("Remind : Your favorite team is playing today ({0})",this.match);
                    }
                    else
                    {
                        reference = PackageHost.GetSettingValue<long>("minBeer");
                        message = "Insufficient beers";
                    }

                    if ((long)e.NewState.Value < reference)
                    {
                        //* Send push bullet alert
                        PackageHost.CreateScope("PushBullet").Proxy.SendPush(
                           new
                           {
                               Title = "Breaking beer !",
                               Message = string.Format("{0} ({1} in stock)", message, e.NewState.Value)
                           });
                    }  
                }
            }
        }
    }
}
