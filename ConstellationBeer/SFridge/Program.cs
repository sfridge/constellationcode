﻿using Constellation;
using Constellation.Host;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SFridge
{
    public class Program : PackageBase
    {
        static void Main(string[] args)
        {
            PackageHost.Start<Program>(args);
        }

        public override void OnStart()
        {
            PackageHost.WriteInfo("Package starting - IsRunning: {0} - IsConnected: {1}", PackageHost.IsRunning, PackageHost.IsConnected);

            Task.Factory.StartNew(() =>
            {
                int sec = 0;
                while (PackageHost.IsRunning)
                {
                    if (sec == PackageHost.GetSettingValue<int>("Interval"))
                    {
                        string fridge_content = GET(PackageHost.GetSettingValue<string>("fridge_url"));
                        PackageHost.PushStateObject<int>("Count", countBeerInFridge(fridge_content));
                        PackageHost.PushStateObject<Object>("Content", fridge_content);
                        PackageHost.PushStateObject<Object>("Tobuy", GET(PackageHost.GetSettingValue<string>("shopping_url")));
                        sec = 0;
                    }
                    sec++;
                    Thread.Sleep(1000);
                }
            });
        }

        public int countBeerInFridge(string fridge_content)
        {
            int count = 0;
            if (fridge_content != null)
            {
                dynamic data = JObject.Parse(fridge_content);
                foreach(var e in data.response){
                    count += (int)e.quantity;
                }
            }
            else
            {
                count = -1;
            }
            return count;
            
        }
        public string GET(string url)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream stream = response.GetResponseStream();
                StreamReader reader = new StreamReader(stream);
                string data = reader.ReadToEnd();
                reader.Close();
                stream.Close();
                return data;
            }
            catch (Exception e)
            {
                PackageHost.WriteInfo("Failed of get request on fridge : \n {0}", e.Message);
            }
            return null;
        }
    }
}
