﻿using Constellation;
using Constellation.Host;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ShoppingList
{
    public class Program : PackageBase
    {
        static void Main(string[] args)
        {
            PackageHost.Start<Program>(args);
        }

        public override void OnStart()
        {
            PackageHost.WriteInfo("Package starting - IsRunning: {0} - IsConnected: {1}", PackageHost.IsRunning, PackageHost.IsConnected);

            Task.Factory.StartNew(() =>
            {
                int sec = 0;
                while (PackageHost.IsRunning)
                {
                    if (sec == PackageHost.GetSettingValue<int>("Interval"))
                    {
                        PackageHost.PushStateObject<Object>("Shop", GET(PackageHost.GetSettingValue<string>("url")));
                        sec = 0;
                    }
                    sec++;
                    Thread.Sleep(1000);
                }
            });
        }

                public static string GET(string url)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream stream = response.GetResponseStream();
                StreamReader reader = new StreamReader(stream);
                string data = reader.ReadToEnd();
                reader.Close();
                stream.Close();
                return data;
            }
            catch (Exception e)
            {
                PackageHost.WriteInfo("Failed of get request on fridge : \n {0}", e.Message);
            }
            return null;
        }
    }
}
