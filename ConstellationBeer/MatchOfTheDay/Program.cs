﻿using Constellation;
using Constellation.Host;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.ServiceModel.Syndication;
using System.Threading;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;

namespace MatchOfTheDay
{
    public class Program : PackageBase
    {
        static void Main(string[] args)
        {
            PackageHost.Start<Program>(args);
        }

        public override void OnStart()
        {
            PackageHost.WriteInfo("Package starting - IsRunning: {0} - IsConnected: {1}", PackageHost.IsRunning, PackageHost.IsConnected);
            PackageHost.PushStateObject("MOTD_favteam", PackageHost.GetSettingValue<string>("team"));
            Task.Factory.StartNew(() =>
            {
                int sec = 0;
                while (PackageHost.IsRunning)
                {
                    if (sec == PackageHost.GetSettingValue<int>("Interval"))
                    {

                        List<String> listMatch = new List<string>();
                        XmlReader reader = XmlReader.Create(PackageHost.GetSettingValue<string>("url"));
                        SyndicationFeed feed = SyndicationFeed.Load(reader);
                        reader.Close();
                        foreach (SyndicationItem item in feed.Items)
                        {
                            String match = item.Title.Text;
                            listMatch.Add(match);
                        }
                        PackageHost.PushStateObject("MOTD", listMatch);
                        sec = 0;
                    }
                    sec++;
                    Thread.Sleep(1000);
                }
            });
        }
    }
}


