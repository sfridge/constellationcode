﻿// Create the client
var constellation = $.signalR.createConstellationClient("http://localhost:8088", "qsdfgh", "My Web Page");

// Attach handler when connected
constellation.connection.stateChanged(function (change) {
    if (change.newState === $.signalR.connectionState.connected) {
        console.log("I'm connected !!!");
        constellation.server.requestStateObjects("*", "SFridge", "Content", "*");
        constellation.server.subscribeStateObjects("*", "SFridge", "Content", "*");

        constellation.server.requestStateObjects("*", "MatchOfTheDay", "MOTD", "*");
        constellation.server.subscribeStateObjects("*", "MatchOfTheDay", "MOTD", "*");

        constellation.server.requestStateObjects("*", "SFridge", "Tobuy", "*");
        constellation.server.subscribeStateObjects("*", "SFridge", "Tobuy", "*");

        constellation.server.requestStateObjects("*", "SFridge", "Count", "*");
        constellation.server.subscribeStateObjects("*", "SFridge", "Count", "*");
    }
});

constellation.client.onUpdateStateObject(function (message) {
    console.log(message);
    switch (message.Name) {
        case "Content":
            onUpdateFridge(message);
            break;
        case "Tobuy":
            onUpdateShoppingList(message);
            break;
        case "MOTD":
            onUpdateMatch(message);
            break;
        case "Count":
            console.log(message.Value)
    }
});

function onUpdateFridge(message) {
    var data = JSON.parse(message.Value);
    $("#fridge_content").html();
    var content = '';
    data.response.forEach(function (e) {
        content += '<p>' + e.name + ' : ' + e.quantity + ' ('+e.volume +')</p>'
    });
    $("#fridge_content").html(content);
    $("#fridge_date").text(message.LastUpdate);
}

function onUpdateShoppingList(message) {
    var data = JSON.parse(message.Value);
    $("#shoppinglist_content").html();
    var content = '';
    data.response.forEach(function (e) {
        content += '<p>' + e.name + ' : ' + e.quantity;
        if (e.must_have) {
            content += ' (must have)';
        }
        content += '</p>';
    });
    $("#shoppinglist_content").html(content);
    $("#shoppinglist_date").text(message.LastUpdate);
}
function onUpdateMatch(message){
    var content = '';
    message.Value.forEach(function (element) {
        content += '<p>'+element+'</p>';
    });
    $("#matches_content").html(content);
    $("#matches_date").text(message.LastUpdate);
}

constellation.connection.start();

